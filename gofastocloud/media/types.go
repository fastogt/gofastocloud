package media

import "gitlab.com/fastogt/gofastogt/gofastogt"

type Bitrate int
type Volume float64

type LoginAndPassword struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}

type StreamLink struct {
	WWE        *LoginAndPassword `json:"wwe,omitempty"`
	HttpProxy  *string           `json:"http_proxy,omitempty"`
	HttpsProxy *string           `json:"https_proxy,omitempty"`
	Prefer     QualityPrefer     `json:"prefer"`
}

type SrtKey struct {
	Passphrase string `json:"passphrase"`
	KeyLen     int    `json:"pbkeylen"`
}

type DrmKey struct {
	Kid string `json:"kid"`
	Key string `json:"key"`
}

type WhipProp struct {
	AuthToken *string `json:"auth_token,omitempty"`
}

type WhepProp struct {
	AuthToken *string `json:"auth_token,omitempty"`
}

type WebRTCProp struct {
	Stun string `json:"stun"`
	Turn string `json:"turn"`
}

type S3Prop struct {
	AccessKey string `json:"access_key"`
	SecretKey string `json:"secret_key"`
}

type KvsProp struct {
	StreamName  string `json:"stream_name"`
	AccessKey   string `json:"access_key"`
	SecretKey   string `json:"secret_key"`
	AwsRegion   string `json:"aws_region"`
	StorageSize int    `json:"storage_size"`
}

type AzureProp struct {
	AccountName string `json:"account_name"`
	AccountKey  string `json:"account_key"`
	Location    string `json:"location"`
}

type GoogleProp struct {
	AccountCreds string `json:"account_creds"`
}

type MlModel struct {
	Path string `json:"path"`
}

type MachineLearning struct {
	Backend  int       `json:"backend"`
	Models   []MlModel `json:"models"`
	Tracking bool      `json:"tracking"`
	Overlay  bool      `json:"overlay"`
}

type Logo struct {
	Path     string          `json:"path"`
	Position gofastogt.Point `json:"position"`
	Alpha    float64         `json:"alpha"`
	Size     gofastogt.Size  `json:"size"`
}

type RSVGLogo struct {
	Path     string          `json:"path"`
	Position gofastogt.Point `json:"position"`
	Size     gofastogt.Size  `json:"size"`
}

type BackgroundEffect struct {
	Type     BackgroundEffectType `json:"type"`
	Strength *float64             `json:"strength,omitempty"`
	Image    *string              `json:"image,omitempty"`
	Color    *int                 `json:"color,omitempty"`
}

type AudioStabilization struct {
	Model GPUModel        `json:"gpu_model"`
	Type  AudioEffectType `json:"type"`
}

type Font struct {
	Family string `json:"family"`
	Size   int    `json:"size"`
}

type TextOverlay struct {
	Text      string  `json:"text"`
	XAbsolute float64 `json:"x_absolute"`
	YAbsolute float64 `json:"y_absolute"`
	Font      *Font   `json:"font,omitempty"`
}

type ExtraConfig map[string]interface{}

type MediaInfo struct {
	Rotation *int                   `json:"rotation,omitempty"`
	Width    int                    `json:"width"`
	Height   int                    `json:"height"`
	Duration gofastogt.DurationMsec `json:"duration"` // msec
}

func (info *MediaInfo) VideoFlip() VideoFlip {
	if info.Rotation == nil {
		return 0
	}

	rotation := *info.Rotation
	return MakeVideoFlipFromRotation(rotation)
}

// Media file info

type MediaUrlInfo struct {
	MediaInfo
	Url string `json:"url"`
}

func NewMediaUrlInfo(info MediaInfo, url string) *MediaUrlInfo {
	minfo := MediaUrlInfo{info, url}
	return &minfo
}

type Programme struct {
	Channel     string                `json:"channel"`
	Start       gofastogt.UtcTimeMsec `json:"start"`
	Stop        gofastogt.UtcTimeMsec `json:"stop"`
	Title       string                `json:"title"`
	Category    *string               `json:"category,omitempty"`
	Description *string               `json:"desc,omitempty"`
	Icon        *string               `json:"icon,omitempty"`
}

type ProgramInfo struct {
	Programme
}
