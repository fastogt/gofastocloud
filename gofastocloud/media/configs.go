package media

import (
	"encoding/json"
	"errors"
	"fmt"

	"gitlab.com/fastogt/gofastogt/gofastogt"
)

type StreamId string

func (sid StreamId) IsValid() bool {
	return len(sid) != 0
}

type BaseConfig struct {
	Id     StreamId    `json:"id"`
	Type   StreamType  `json:"type"`
	Output []OutputUri `json:"output"`
}

func (base *BaseConfig) UnmarshalJSON(data []byte) error {
	required := struct {
		Id     *StreamId   `json:"id"`
		Type   *StreamType `json:"type"`
		Output []OutputUri `json:"output"`
	}{}
	err := json.Unmarshal(data, &required)
	if err != nil {
		return err
	}

	if required.Id == nil {
		return errors.New("id field required")
	}

	sid := *required.Id
	if !sid.IsValid() {
		return errors.New("invalid id field")
	}

	if required.Type == nil {
		return errors.New("type field required")
	}

	st := *required.Type
	if !st.IsValid() {
		return errors.New("invalid type field")
	}

	base.Id = sid
	base.Type = st
	base.Output = required.Output
	return nil
}

type ProxyConfig struct {
	BaseConfig
}

func NewProxyConfig(sid StreamId, output []OutputUri) ProxyConfig {
	return ProxyConfig{BaseConfig: BaseConfig{Id: sid, Type: STREAM_TYPE_PROXY, Output: output}}
}

type RawHardwareConfig struct {
	BaseConfig
	FeedbackDir     string         `json:"feedback_directory"` //+
	DataDir         string         `json:"data_directory"`     //
	LogLevel        StreamLogLevel `json:"log_level"`          //+
	Loop            bool           `json:"loop"`               //
	SelectedInput   int            `json:"selected_input"`     //
	RestartAttempts int            `json:"restart_attempts"`   //
	Input           []InputUri     `json:"input"`              //

	AutoExitTime *StreamTTL   `json:"auto_exit_time,omitempty"` //
	ExtraConfig  *ExtraConfig `json:"extra_config,omitempty"`   //+
}

type HardwareConfig struct {
	RawHardwareConfig

	HaveVideo bool `json:"have_video"`
	HaveAudio bool `json:"have_audio"`

	AudioTracksCount int  `json:"audio_tracks_count"`
	AudioSelect      *int `json:"audio_select,omitempty"`
}

type RelayConfig struct {
	HardwareConfig
	VideoParser *VideoParser `json:"video_parser,omitempty"`
	AudioParser *AudioParser `json:"audio_parser,omitempty"`
}

func MakeRelayConfig(sid StreamId, output []OutputUri, videoParser *VideoParser, audioParser *AudioParser, feedbackDir string, dataDir string, logLevel StreamLogLevel, loop bool, haveVideo bool,
	haveAudio bool, restartAttempts int,
	autoExitTime *StreamTTL, input []InputUri, audioSelect *int, audioTracksCount int) RelayConfig {
	base := BaseConfig{Id: sid, Type: STREAM_TYPE_RELAY, Output: output}
	raw := RawHardwareConfig{BaseConfig: base, FeedbackDir: feedbackDir, DataDir: dataDir, LogLevel: logLevel, Loop: loop, AutoExitTime: autoExitTime, RestartAttempts: restartAttempts, Input: input}
	hard := HardwareConfig{RawHardwareConfig: raw, HaveVideo: haveVideo, HaveAudio: haveAudio, AudioSelect: audioSelect, AudioTracksCount: audioTracksCount}
	return RelayConfig{HardwareConfig: hard, AudioParser: audioParser, VideoParser: videoParser}
}

type AlphaMethod struct {
	Method AlphaMethodType `json:"method"`
	Alpha  *float64        `json:"alpha,omitempty"`
	Color  *int            `json:"color,omitempty"`
}

type Wpe struct {
	GL bool `json:"gl"`
}

type Cef struct {
	GPU bool `json:"gpu"`
}

type OverlayUrl struct {
	Url  string         `json:"url"`
	Type OverlayUrlType `json:"type"`

	Wpe *Wpe `json:"wpe,omitempty"`
	Cef *Cef `json:"cef,omitempty"`
}

type StreamOverlay struct {
	Url *OverlayUrl `json:"url"`
	//
	Background *BackgroundColor `json:"background,omitempty"`
	Method     *AlphaMethod     `json:"method,omitempty"`
	Size       *gofastogt.Size  `json:"size,omitempty"`
}

type StreamTTL struct {
	TTL     int  `json:"ttl"`
	Phoenix bool `json:"phoenix"`
}

type EncodeConfig struct {
	HardwareConfig
	RelayAudio bool       `json:"relay_audio"`
	RelayVideo bool       `json:"relay_video"`
	VideoCodec VideoCodec `json:"video_codec"`
	AudioCodec AudioCodec `json:"audio_codec"`

	// audio optional
	Resample           *bool               `json:"resample,omitempty"`
	AudioChannelsCount *int                `json:"audio_channels_count,omitempty"`
	Volume             *Volume             `json:"volume,omitempty"`
	AudioBitrate       *Bitrate            `json:"audio_bitrate,omitempty"`
	AudioStabilization *AudioStabilization `json:"audio_stabilization,omitempty"`

	// video optional
	Deinterlace      *bool               `json:"deinterlace,omitempty"`
	FrameRate        *gofastogt.Rational `json:"frame_rate,omitempty"`
	Size             *gofastogt.Size     `json:"size,omitempty"`
	MachineLearning  *MachineLearning    `json:"machine_learning,omitempty"`
	VideoBitrate     *Bitrate            `json:"video_bitrate,omitempty"`
	Logo             *Logo               `json:"logo,omitempty"`
	RsvgLogo         *RSVGLogo           `json:"rsvg_logo,omitempty"`
	AspectRatio      *gofastogt.Rational `json:"aspect_ratio,omitempty"`
	BackgroundEffect *BackgroundEffect   `json:"background_effect,omitempty"`
	TextOverlay      *TextOverlay        `json:"text_overlay,omitempty"`
	VideoFlip        *VideoFlip          `json:"video_flip,omitempty"`
	StreamOverlay    *StreamOverlay      `json:"stream_overlay,omitempty"`
}

func MakeEncodeConfig(sid StreamId, output []OutputUri, feedbackDir string, dataDir string, logLevel StreamLogLevel, loop bool, haveVideo bool, haveAudio bool, restartAttempts int, autoExitTime *StreamTTL, input []InputUri, audioSelect *int, audioTracksCount int, relayAudio bool, relayVideo bool, deinterlace *bool, resample *bool, volume *Volume, videoCodec VideoCodec, audioCodec AudioCodec, frameRate *gofastogt.Rational, audioChannelsCount *int, size *gofastogt.Size, machineLearning *MachineLearning, videoBitrate *Bitrate, audioBitrate *Bitrate, logo *Logo, rsvgLogo *RSVGLogo, aspectRatio *gofastogt.Rational, backgroundEffect *BackgroundEffect, textOverlay *TextOverlay, videoFlip *VideoFlip) EncodeConfig {
	base := BaseConfig{Id: sid, Type: STREAM_TYPE_ENCODE, Output: output}
	raw := RawHardwareConfig{BaseConfig: base, FeedbackDir: feedbackDir, DataDir: dataDir, LogLevel: logLevel, Loop: loop, AutoExitTime: autoExitTime, RestartAttempts: restartAttempts, Input: input}
	hard := HardwareConfig{RawHardwareConfig: raw, HaveVideo: haveVideo, HaveAudio: haveAudio, AudioSelect: audioSelect, AudioTracksCount: audioTracksCount}
	return EncodeConfig{HardwareConfig: hard, RelayAudio: relayAudio, RelayVideo: relayVideo, Deinterlace: deinterlace, Resample: resample, Volume: volume, VideoCodec: videoCodec, AudioCodec: audioCodec, FrameRate: frameRate, AudioChannelsCount: audioChannelsCount, Size: size, MachineLearning: machineLearning, VideoBitrate: videoBitrate, AudioBitrate: audioBitrate, Logo: logo, RsvgLogo: rsvgLogo, AspectRatio: aspectRatio, BackgroundEffect: backgroundEffect, TextOverlay: textOverlay, VideoFlip: videoFlip}
}

type TimeShiftRecordConfig struct {
	RelayConfig
	TimeshiftChunkDuration int    `json:"timeshift_chunk_duration"`
	TimeshiftChunkLifeTime int    `json:"timeshift_chunk_life_time"`
	TimeshiftDir           string `json:"timeshift_dir"`
}

type CatchupConfig struct {
	TimeShiftRecordConfig
}

type TimeshiftPlayerConfig struct {
	RelayConfig
	TimeshiftDir   string `json:"timeshift_dir"`
	TimeshiftDelay int    `json:"timeshift_delay"`
}

type TestLifeConfig struct {
	RelayConfig
}

type CodRelayConfig struct {
	RelayConfig
}

type CodEncodeConfig struct {
	EncodeConfig
}

type ProxyVodConfig struct {
	ProxyConfig
}

type VodRelayConfig struct {
	RelayConfig
}

func MakeVodRelayConfig(sid StreamId, output []OutputUri, videoParser *VideoParser, audioParser *AudioParser, feedbackDir string, dataDir string, logLevel StreamLogLevel, loop bool, haveVideo bool,
	haveAudio bool, restartAttempts int,
	autoExitTime *StreamTTL, input []InputUri, audioSelect *int, audioTracksCount int) VodRelayConfig {
	relay := MakeRelayConfig(sid, output, videoParser, audioParser, feedbackDir, dataDir, logLevel, loop, haveVideo, haveAudio, restartAttempts, autoExitTime, input, audioSelect, audioTracksCount)
	relay.Type = STREAM_TYPE_VOD_RELAY
	return VodRelayConfig{RelayConfig: relay}
}

type VodEncodeConfig struct {
	EncodeConfig
}

func MakeVodEncodeConfig(sid StreamId, output []OutputUri, feedbackDir string, dataDir string, logLevel StreamLogLevel, loop bool, haveVideo bool, haveAudio bool, restartAttempts int, autoExitTime *StreamTTL, input []InputUri, audioSelect *int, audioTracksCount int, relayAudio bool, relayVideo bool, deinterlace *bool, resample *bool, volume *Volume, videoCodec VideoCodec, audioCodec AudioCodec, frameRate *gofastogt.Rational, audioChannelsCount *int, size *gofastogt.Size, machineLearning *MachineLearning, videoBitrate *Bitrate, audioBitrate *Bitrate, logo *Logo, rsvgLogo *RSVGLogo, aspectRatio *gofastogt.Rational, backgroundEffect *BackgroundEffect, textOverlay *TextOverlay, videoFlip *VideoFlip) VodEncodeConfig {
	encode := MakeEncodeConfig(sid, output, feedbackDir, dataDir, logLevel, loop, haveVideo, haveAudio, restartAttempts, autoExitTime, input, audioSelect, audioTracksCount, relayAudio, relayVideo, deinterlace, resample, volume, videoCodec, audioCodec, frameRate, audioChannelsCount, size, machineLearning, videoBitrate, audioBitrate, logo, rsvgLogo, aspectRatio, backgroundEffect, textOverlay, videoFlip)
	encode.Type = STREAM_TYPE_VOD_ENCODE
	return VodEncodeConfig{EncodeConfig: encode}
}

type EventConfig struct {
	VodEncodeConfig
}

type CvDataConfig struct {
	EncodeConfig
}

type ChangerRelayConfig struct {
	RelayConfig
}

type ChangerEncodeConfig struct {
	EncodeConfig
}

type LiteStreamConfig struct {
	RawHardwareConfig
}

type MosaicStreamConfig struct {
	EncodeConfig
}

func ParseTimeshiftPlayerConfig(data []byte) (*TimeshiftPlayerConfig, error) {
	var timeshiftPlayer TimeshiftPlayerConfig
	err := json.Unmarshal(data, &timeshiftPlayer)
	if err != nil {
		return nil, err
	}

	return &timeshiftPlayer, nil
}

func ParseTimeshiftRecorderConfig(data []byte) (*TimeShiftRecordConfig, error) {
	var timeshiftRecorder TimeShiftRecordConfig
	err := json.Unmarshal(data, &timeshiftRecorder)
	if err != nil {
		return nil, err
	}

	return &timeshiftRecorder, nil
}

func ParseCatchupConfig(data []byte) (*CatchupConfig, error) {
	var catchup CatchupConfig
	err := json.Unmarshal(data, &catchup)
	if err != nil {
		return nil, err
	}

	return &catchup, nil
}

func ParseTestLifeConfig(data []byte) (*TestLifeConfig, error) {
	var testLife TestLifeConfig
	err := json.Unmarshal(data, &testLife)
	if err != nil {
		return nil, err
	}

	return &testLife, nil
}

func ParseVodRelayConfig(data []byte) (*VodRelayConfig, error) {
	var vodRelay VodRelayConfig
	err := json.Unmarshal(data, &vodRelay)
	if err != nil {
		return nil, err
	}

	return &vodRelay, nil
}

func ParseVodEncodeConfig(data []byte) (*VodEncodeConfig, error) {
	var vodEncode VodEncodeConfig
	err := json.Unmarshal(data, &vodEncode)
	if err != nil {
		return nil, err
	}

	return &vodEncode, nil
}

func ParseCodRelayConfig(data []byte) (*CodRelayConfig, error) {
	var codRelay CodRelayConfig
	err := json.Unmarshal(data, &codRelay)
	if err != nil {
		return nil, err
	}

	return &codRelay, nil
}

func ParseCodEncodeConfig(data []byte) (*CodEncodeConfig, error) {
	var codEncode CodEncodeConfig
	err := json.Unmarshal(data, &codEncode)
	if err != nil {
		return nil, err
	}

	return &codEncode, nil
}

func ParseEventConfig(data []byte) (*EventConfig, error) {
	var event EventConfig
	err := json.Unmarshal(data, &event)
	if err != nil {
		return nil, err
	}

	return &event, nil
}

func ParseCvDataConfig(data []byte) (*CvDataConfig, error) {
	var cv CvDataConfig
	err := json.Unmarshal(data, &cv)
	if err != nil {
		return nil, err
	}

	return &cv, nil
}

func ParseChangerEncodeConfig(data []byte) (*ChangerEncodeConfig, error) {
	var result ChangerEncodeConfig
	err := json.Unmarshal(data, &result)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

func ParseChangerRelayConfig(data []byte) (*ChangerRelayConfig, error) {
	var result ChangerRelayConfig
	err := json.Unmarshal(data, &result)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

func ParseLiteStreamConfig(data []byte) (*LiteStreamConfig, error) {
	var result LiteStreamConfig
	err := json.Unmarshal(data, &result)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

func ParseMosaicStreamConfig(data []byte) (*MosaicStreamConfig, error) {
	var result MosaicStreamConfig
	err := json.Unmarshal(data, &result)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

func ParseBaseConfig(data []byte) (*BaseConfig, error) {
	var base BaseConfig
	err := json.Unmarshal(data, &base)
	if err != nil {
		return nil, err
	}

	return &base, nil
}

func ParseProxyConfig(data []byte) (*ProxyConfig, error) {
	var proxy ProxyConfig
	err := json.Unmarshal(data, &proxy)
	if err != nil {
		return nil, err
	}

	return &proxy, nil
}

func ParseVodProxyConfig(data []byte) (*ProxyVodConfig, error) {
	var proxy ProxyVodConfig
	err := json.Unmarshal(data, &proxy)
	if err != nil {
		return nil, err
	}

	return &proxy, nil
}

func ParseRestreamConfig(data []byte) (*RelayConfig, error) {
	var relay RelayConfig
	err := json.Unmarshal(data, &relay)
	if err != nil {
		return nil, err
	}

	return &relay, nil
}

func ParseEncodeConfig(data []byte) (*EncodeConfig, error) {
	var encode EncodeConfig
	err := json.Unmarshal(data, &encode)
	if err != nil {
		return nil, err
	}

	return &encode, nil
}

func MakeFeedbackDir(path string, sid StreamId) string {
	ln := len(path)
	if ln != 0 && path[ln-1] != '/' {
		return fmt.Sprintf("%s/%s", path, sid)
	}

	return fmt.Sprintf("%s/%s", path, sid)
}

func MakeTimeshiftDir(path string, sid StreamId) string {
	ln := len(path)
	if ln != 0 && path[ln-1] != '/' {
		return fmt.Sprintf("%s/%s", path, sid)
	}

	return fmt.Sprintf("%s%s", path, sid)
}

func MakeDataDir(path string, sid StreamId) string {
	ln := len(path)
	if ln != 0 && path[ln-1] != '/' {
		return fmt.Sprintf("%s/%s", path, sid)
	}

	return fmt.Sprintf("%s/%s", path, sid)
}

/*func MakeTimeshiftHttpRoot(streamType StreamType, sid StreamId, oid int) string {
	return fmt.Sprintf("~/streamer/timeshifts/%d/%s/%d", streamType, sid, oid)
}*/

func MakeHttpRootFull(path string, streamType StreamType, sid StreamId, oid int) string {
	ln := len(path)
	if ln != 0 && path[ln-1] != '/' {
		return fmt.Sprintf("%s/%d/%s/%d", path, streamType, sid, oid)
	}
	return fmt.Sprintf("%s%d/%s/%d", path, streamType, sid, oid)
}
