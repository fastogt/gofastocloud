package media

import (
	"encoding/json"
	"net/http"
	"net/url"

	"github.com/gorilla/websocket"
)

type IWsClient interface {
	OnStreamStatisticReceived(client *FastoCloud, old *StreamStatisticInfo, statistic StreamStatisticInfo)
	OnStreamSourcesChanged(client *FastoCloud, source ChangedSourcesInfo)
	OnStreamMlNotification(client *FastoCloud, notify MlNotificationInfo)
	OnStreamResultReady(client *FastoCloud, result ResultStreamInfo)
	OnQuitStatusStream(client *FastoCloud, status QuitStatusInfo)

	OnWebRTCOutInitReceived(client *FastoCloud, web WebRTCOutInitInfo)
	OnWebRTCOutDeInitReceived(client *FastoCloud, web WebRTCOutDeInitInfo)
	OnWebRTCOutSdpReceived(client *FastoCloud, web WebRTCOutSdpInfo)
	OnWebRTCOutIceReceived(client *FastoCloud, web WebRTCOutIceInfo)
	OnWebRTCInInitReceived(client *FastoCloud, web WebRTCInInitInfo)
	OnWebRTCInDeInitReceived(client *FastoCloud, web WebRTCInDeInitInfo)
	OnWebRTCInSdpReceived(client *FastoCloud, web WebRTCInSdpInfo)
	OnWebRTCInIceReceived(client *FastoCloud, web WebRTCInIceInfo)

	OnServiceStatisticReceived(client *FastoCloud, statistic ServiceStatisticInfo)
	OnCDNStatisticReceived(client *FastoCloud, statistic CDNStatisticInfo)

	OnStarted(fasto *FastoCloud)
	OnFinished(fasto *FastoCloud, err error)
}

func (fasto *FastoCloud) Run(client IWsClient) {
	u, err := url.Parse(fasto.generateRoute("/updates"))
	if err != nil {
		return
	}

	requestHeader := http.Header{}
	if fasto.api != nil {
		values := u.Query()
		api := string(*fasto.api)
		values.Add("API-KEY", api)
		u.RawQuery = values.Encode()
		// headers
		requestHeader.Set("API-KEY", api)
	}

	if fasto.realIP != nil {
		requestHeader.Set("X-Real-IP", *fasto.realIP)
	}

	if u.Scheme == "https" {
		u.Scheme = "wss"
	} else {
		u.Scheme = "ws"
	}

	c, _, err := websocket.DefaultDialer.Dial(u.String(), requestHeader)
	if err != nil {
		return
	}

	defer c.Close()

	client.OnStarted(fasto)
	var erriner error
	for !fasto.isStopped {
		_, message, erriner := c.ReadMessage()
		if erriner != nil {
			break
		}

		erriner = fasto.processUpdate(message, client)
		if erriner != nil {
			break
		}
	}
	client.OnFinished(fasto, erriner)
}

func (fasto *FastoCloud) Stop() {
	fasto.isStopped = true
}

type wsMessage struct {
	Type string          `json:"type"`
	Data json.RawMessage `json:"data"`
}

func (fasto *FastoCloud) processUpdate(rawMessage []byte, client IWsClient) error {
	var msg wsMessage
	err := json.Unmarshal(rawMessage, &msg)
	if err != nil {
		return err
	}

	if msg.Type == kStatisticServiceBroadcastCommand {
		var stat ServiceStatisticInfo
		err := json.Unmarshal(msg.Data, &stat)
		if err != nil {
			return err
		}
		if fasto.info != nil {
			fasto.info.Runtime = stat
		}
		client.OnServiceStatisticReceived(fasto, stat)
	} else if msg.Type == kQuitStatusStreamBroadcastCommand {
		var quit QuitStatusInfo
		err := json.Unmarshal(msg.Data, &quit)
		if err != nil {
			return err
		}
		fasto.deleteActiveStream(quit.Id)
		client.OnQuitStatusStream(fasto, quit)
	} else if msg.Type == kStatisticStreamBroadcastCommand {
		var stat StreamStatisticInfo
		err := json.Unmarshal(msg.Data, &stat)
		if err != nil {
			return err
		}
		old := fasto.updateActiveStream(stat)
		client.OnStreamStatisticReceived(fasto, old, stat)
	} else if msg.Type == kMlNotificationStreamBroadcastCommand {
		var notif MlNotificationInfo
		err := json.Unmarshal(msg.Data, &notif)
		if err != nil {
			return err
		}
		client.OnStreamMlNotification(fasto, notif)
	} else if msg.Type == kStatisticCDNBroadcastCommand {
		var stat CDNStatisticInfo
		err := json.Unmarshal(msg.Data, &stat)
		if err != nil {
			return err
		}
		client.OnCDNStatisticReceived(fasto, stat)
	} else if msg.Type == kChangedStreamBroadcastCommand {
		var source ChangedSourcesInfo
		if err := json.Unmarshal(msg.Data, &source); err == nil {
			client.OnStreamSourcesChanged(fasto, source)
		}
	} else if msg.Type == kResultStreamBroadcastCommand {
		var res ResultStreamInfo
		if err := json.Unmarshal(msg.Data, &res); err == nil {
			client.OnStreamResultReady(fasto, res)
		}
	} else if msg.Type == kWebRTCOutInitStreamBroadcastCommand {
		var web WebRTCOutInitInfo
		if err := json.Unmarshal(msg.Data, &web); err == nil {
			client.OnWebRTCOutInitReceived(fasto, web)
		}
	} else if msg.Type == kWebRTCOutDeInitStreamBroadcastCommand {
		var web WebRTCOutDeInitInfo
		if err := json.Unmarshal(msg.Data, &web); err == nil {
			client.OnWebRTCOutDeInitReceived(fasto, web)
		}
	} else if msg.Type == kWebRTCOutSdpStreamBroadcastCommand {
		var web WebRTCOutSdpInfo
		if err := json.Unmarshal(msg.Data, &web); err == nil {
			client.OnWebRTCOutSdpReceived(fasto, web)
		}
	} else if msg.Type == kWebRTCOutIceStreamBroadcastCommand {
		var web WebRTCOutIceInfo
		if err := json.Unmarshal(msg.Data, &web); err == nil {
			client.OnWebRTCOutIceReceived(fasto, web)
		}
	} else if msg.Type == kWebRTCInInitStreamBroadcastCommand {
		var web WebRTCInInitInfo
		if err := json.Unmarshal(msg.Data, &web); err == nil {
			client.OnWebRTCInInitReceived(fasto, web)
		}
	} else if msg.Type == kWebRTCInDeInitStreamBroadcastCommand {
		var web WebRTCInDeInitInfo
		if err := json.Unmarshal(msg.Data, &web); err == nil {
			client.OnWebRTCInDeInitReceived(fasto, web)
		}
	} else if msg.Type == kWebRTCInSdpStreamBroadcastCommand {
		var web WebRTCInSdpInfo
		if err := json.Unmarshal(msg.Data, &web); err == nil {
			client.OnWebRTCInSdpReceived(fasto, web)
		}
	} else if msg.Type == kWebRTCInIceStreamBroadcastCommand {
		var web WebRTCInIceInfo
		if err := json.Unmarshal(msg.Data, &web); err == nil {
			client.OnWebRTCInIceReceived(fasto, web)
		}
	}

	return nil
}

func (fasto *FastoCloud) updateActiveStream(statistic StreamStatisticInfo) *StreamStatisticInfo {
	var old *StreamStatisticInfo
	fasto.activeStreamsMutex.Lock()
	sid := statistic.Id
	if val, ok := fasto.activeStreams[sid]; ok {
		fasto.activeStreams[sid] = NewStreamInfo(sid, val.Request, &statistic)
		old = val.Statistic
	} else {
		fasto.activeStreams[sid] = NewStreamInfo(sid, nil, &statistic)
		old = nil
	}
	fasto.activeStreamsMutex.Unlock()
	return old
}
