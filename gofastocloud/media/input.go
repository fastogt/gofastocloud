package media

import (
	"encoding/json"
	"errors"
	"net/url"

	"gitlab.com/fastogt/gofastogt/gofastogt"
)

const kWebRTCInUrl = "unknown://webrtc"

type InputUrl struct {
	Id  int    `json:"id"`
	Uri string `json:"uri"`
}

func (i *InputUrl) Scheme() (*string, error) {
	u, err := url.Parse(i.Uri)
	if err != nil {
		return nil, err
	}
	if u.Scheme == "" {
		return nil, errors.New("url schema is required")
	}
	return &u.Scheme, nil
}

func (input *InputUrl) UnmarshalJSON(data []byte) error {
	required := struct {
		Id  *int    `json:"id"`
		Uri *string `json:"uri"`
	}{}
	err := json.Unmarshal(data, &required)
	if err != nil {
		return err
	}
	if required.Id == nil {
		return errors.New("id field required")
	}
	if *required.Id < 0 {
		return errors.New("invalid id")
	}
	if required.Uri == nil {
		return errors.New("uri field required")
	}
	if len(*required.Uri) == 0 {
		return errors.New("invalid uri")
	}

	input.Id = *required.Id
	input.Uri = *required.Uri
	return nil
}

// FIXME: need UnmarshalJSON
type InputUriData struct {
	// http
	UserAgent  *UserAgent  `json:"user_agent,omitempty"`
	StreamLink *StreamLink `json:"stream_link,omitempty"`
	Proxy      *string     `json:"proxy,omitempty"`
	Wpe        *Wpe        `json:"wpe,omitempty"`
	Cef        *Cef        `json:"cef,omitempty"`
	Keys       []DrmKey    `json:"keys,omitempty"`
	Whep       *WhepProp   `json:"whep,omitempty"`
	// udp
	ProgramNumber  *int    `json:"program_number,omitempty"`
	MulticastIface *string `json:"multicast_iface,omitempty"`
	// srt
	SrtMode *SrtMode `json:"srt_mode,omitempty"`
	SrtKey  *SrtKey  `json:"srt_key,omitempty"`
	// rtmp
	RtmpSrcType *RtmpSrcType `json:"rtmpsrc_type,omitempty"`
	// ndi
	Ndi *NDIProp `json:"ndi,omitempty"`

	Aws *S3Prop `json:"aws,omitempty"`
	// webrtc
	WebRTC *WebRTCProp `json:"webrtc,omitempty"`
}

func (input *InputUriData) UnmarshalJSON(data []byte) error {
	req := struct {
		UserAgent      *UserAgent   `json:"user_agent,omitempty"`
		StreamLink     *StreamLink  `json:"stream_link,omitempty"`
		Proxy          *string      `json:"proxy,omitempty"`
		Wpe            *Wpe         `json:"wpe,omitempty"`
		Cef            *Cef         `json:"cef,omitempty"`
		Keys           []DrmKey     `json:"keys,omitempty"`
		Whep           *WhepProp    `json:"whep,omitempty"`
		ProgramNumber  *int         `json:"program_number,omitempty"`
		MulticastIface *string      `json:"multicast_iface,omitempty"`
		SrtMode        *SrtMode     `json:"srt_mode,omitempty"`
		SrtKey         *SrtKey      `json:"srt_key,omitempty"`
		RtmpSrcType    *RtmpSrcType `json:"rtmpsrc_type,omitempty"`
		Ndi            *NDIProp     `json:"ndi,omitempty"`
		Aws            *S3Prop      `json:"aws,omitempty"`
		WebRTC         *WebRTCProp  `json:"webrtc,omitempty"`
	}{}
	err := json.Unmarshal(data, &req)
	if err != nil {
		return err
	}
	if req.UserAgent != nil {
		if !req.UserAgent.IsValid() {
			return errors.New("not correct field user_agent")
		}
		input.UserAgent = req.UserAgent
	}
	if req.StreamLink != nil {
		if !req.StreamLink.Prefer.IsValid() {
			return errors.New("not correct field prefer")
		}
		input.StreamLink = req.StreamLink
	}
	if req.Proxy != nil {
		input.Proxy = req.Proxy
	}
	if req.Wpe != nil {
		input.Wpe = req.Wpe
	}
	if req.Cef != nil {
		input.Cef = req.Cef
	}
	if req.Keys != nil {
		input.Keys = req.Keys
	}
	if req.Whep != nil {
		input.Whep = req.Whep
	}

	if req.ProgramNumber != nil {
		input.ProgramNumber = req.ProgramNumber
	}
	if req.MulticastIface != nil {
		input.MulticastIface = req.MulticastIface
	}
	if req.SrtKey != nil {
		input.SrtKey = req.SrtKey
	}
	if req.RtmpSrcType != nil {
		if !req.RtmpSrcType.IsValid() {
			return errors.New("not correct field rtmpsrc_type")
		}
		input.RtmpSrcType = req.RtmpSrcType
	}
	if req.Ndi != nil {
		input.Ndi = req.Ndi
	}
	if req.Aws != nil {
		input.Aws = req.Aws
	}
	if req.WebRTC != nil {
		input.WebRTC = req.WebRTC
	}
	if req.SrtMode != nil {
		if !req.SrtMode.IsValid() {
			return errors.New("not correct field srt_mode")
		}
		input.SrtMode = req.SrtMode
	}
	return nil
}

type InputUri struct {
	InputUrl
	InputUriData
}

func (input *InputUri) UnmarshalJSON(data []byte) error {
	err := input.InputUrl.UnmarshalJSON(data)
	if err != nil {
		return err
	}

	err = json.Unmarshal(data, &input.InputUriData)
	if err != nil {
		return err
	}

	return nil
}

func NewWebRTCInputUri(oid int, prop *WebRTCProp) *InputUri {
	web := NewInputUri(oid, kWebRTCInUrl)
	web.WebRTC = prop
	return web
}

func MakeFileInputUri(oid int, path string) (*InputUri, error) {
	file, err := gofastogt.MakeFilePathProtocol(path)
	if err != nil {
		return nil, err
	}

	return NewInputUri(oid, file.GetFilePath()), nil
}

func NewInputUri(oid int, url string) *InputUri {
	uri := InputUrl{Id: oid, Uri: url}
	return &InputUri{InputUrl: uri}
}
