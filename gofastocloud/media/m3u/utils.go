package m3u

import (
	"errors"
	"regexp"
	"strconv"
	"strings"
)

// A Tag is a simple key/value pair
type Tag struct {
	Name  string
	Value string
}

// Track represents an m3u track with a Name, Lengh, URI and a set of tags
type Track struct {
	Name   string
	Length int
	URI    string
	Tags   []Tag
}

func (track *Track) GetTVGLogo() *string {
	for _, tag := range track.Tags {
		if tag.Name == "tvg-logo" {
			return &tag.Value
		}
	}
	return nil
}

func (track *Track) GetTVGID() *string {
	for _, tag := range track.Tags {
		if tag.Name == "tvg-id" {
			return &tag.Value
		}
	}
	return nil
}

func (track *Track) GetGroup() *string {
	for _, tag := range track.Tags {
		if tag.Name == "group-title" || tag.Name == "tvg-group" {
			return &tag.Value
		}
	}
	return nil
}

func (track *Track) GetTVGName() *string {
	for _, tag := range track.Tags {
		if tag.Name == "tvg-name" {
			return &tag.Value
		}
	}
	return nil
}

// Playlist is a type that represents an m3u playlist containing 0 or more tracks
type Playlist struct {
	Tracks []Track
}

func ParseToPlaylist(data string) (*Playlist, error) {
	tagsRegExp, _ := regexp.Compile("([a-zA-Z0-9-]+?)=\"([^\"]+)\"")
	onFirstLine := true
	playlists := strings.Split(data, "\n")
	playlist := Playlist{}

	for i := 0; i < len(playlists); i++ {
		line := playlists[i]
		if onFirstLine && !strings.HasPrefix(line, "#EXTM3U") {
			return nil,
				errors.New("invalid m3u file format. Expected #EXTM3U file header")
		}

		onFirstLine = false

		if strings.HasPrefix(line, "#EXTINF") {
			line := strings.Replace(line, "#EXTINF:", "", -1)
			trackInfo := strings.Split(line, ",")
			if len(trackInfo) < 2 {
				return nil,
					errors.New("invalid m3u file format. Expected EXTINF metadata to contain track length and name data")
			}
			length, parseErr := strconv.Atoi(strings.Split(trackInfo[0], " ")[0])
			if parseErr != nil {
				return nil, errors.New("unable to parse length")
			}
			track := &Track{strings.Trim(trackInfo[1], " "), length, "", nil}
			track.Name = strings.TrimSuffix(track.Name, "\r")
			if track.Name == "" {
				track.Name = "Unknown"
			}
			tagList := tagsRegExp.FindAllString(line, -1)
			for i := range tagList {
				tagInfo := strings.Split(tagList[i], "=")
				key := tagInfo[0]
				value := tagInfo[1]
				for i := 2; i < len(tagInfo); i++ {
					value += "=" + tagInfo[i]
				}
				tag := &Tag{key, strings.Replace(value, "\"", "", -1)}
				track.Tags = append(track.Tags, *tag)
			}
			playlist.Tracks = append(playlist.Tracks, *track)
		} else if strings.HasPrefix(line, "#") || line == "" {
			continue
		} else if len(playlist.Tracks) == 0 {
			return nil, errors.New("URI provided for playlist with no tracks")
		} else {
			uri := strings.Trim(line, " ")
			playlist.Tracks[len(playlist.Tracks)-1].URI = strings.TrimSuffix(uri, "\r")
		}
	}

	return &playlist, nil
}
