package media

// WebRTC out init

type WebRTCOutInitRequest struct {
	Sid  StreamId             `json:"id"`
	Init WebRTCSubInitRequest `json:"init"`
}

type WebRTCSubInitRequest struct {
	ConnectionId string      `json:"connection_id"`
	WebRTC       *WebRTCProp `json:"webrtc,omitempty"`
}

func NewWebRTCOutInitRequest(sid StreamId, init WebRTCSubInitRequest) *WebRTCOutInitRequest {
	return &WebRTCOutInitRequest{sid, init}
}

type WebRTCOutInit struct {
}

type WebRTCOutInitResponse struct {
	Data WebRTCOutInit `json:"data"`
}

// WebRTC out deinit

type WebRTCOutDeInitRequest struct {
	Sid    StreamId               `json:"id"`
	DeInit WebRTCSubDeInitRequest `json:"init"`
}

type WebRTCSubDeInitRequest struct {
	ConnectionId string `json:"connection_id"`
}

func NewWebRTCOutDeInitRequest(sid StreamId, deinit WebRTCSubDeInitRequest) *WebRTCOutDeInitRequest {
	return &WebRTCOutDeInitRequest{sid, deinit}
}

type WebRTCOutDeInit struct {
}

type WebRTCOutDeInitResponse struct {
	Data WebRTCOutDeInit `json:"data"`
}

// WebRTC out sdp

type WebRTCOutSdpRequest struct {
	Sid         StreamId           `json:"id"`
	Description SessionDescription `json:"description"`
}

type SessionDescription struct {
	ConnectionId   string `json:"connection_id"`
	SdpDescription string `json:"sdp"`
	SdpType        string `json:"type"`
}

func NewWebRTCOutSdpRequest(sid StreamId, description SessionDescription) *WebRTCOutSdpRequest {
	return &WebRTCOutSdpRequest{sid, description}
}

type WebRTCOutSdp struct {
}

type WebRTCOutSdpResponse struct {
	Data WebRTCOutSdp `json:"data"`
}

// WebRTC out ice

type WebRTCOutIceRequest struct {
	Sid StreamId     `json:"id"`
	Ice IceCandidate `json:"ice"`
}

type IceCandidate struct {
	ConnectionId string `json:"connection_id"`
	Candidate    string `json:"candidate"`
	Mlindex      int    `json:"sdpMLineIndex"`
	Mid          string `json:"sdpMid"`
}

func NewWebRTCOutIceRequest(sid StreamId, ice IceCandidate) *WebRTCOutIceRequest {
	return &WebRTCOutIceRequest{sid, ice}
}

type WebRTCOutIce struct {
}

type WebRTCOutIceResponse struct {
	Data WebRTCOutIce `json:"data"`
}

// WebRTC in init

type WebRTCInInitRequest struct {
	Sid  StreamId             `json:"id"`
	Init WebRTCSubInitRequest `json:"init"`
}

func NewWebRTCInInitRequest(sid StreamId, init WebRTCSubInitRequest) *WebRTCInInitRequest {
	return &WebRTCInInitRequest{sid, init}
}

type WebRTCInInit struct {
}

type WebRTCInInitResponse struct {
	Data WebRTCInInit `json:"data"`
}

// WebRTC in deinit

type WebRTCInDeInitRequest struct {
	Sid    StreamId               `json:"id"`
	DeInit WebRTCSubDeInitRequest `json:"init"`
}

func NewWebRTCInDeInitRequest(sid StreamId, deinit WebRTCSubDeInitRequest) *WebRTCInDeInitRequest {
	return &WebRTCInDeInitRequest{sid, deinit}
}

type WebRTCInDeInit struct {
}

type WebRTCInDeInitResponse struct {
	Data WebRTCInDeInit `json:"data"`
}

// WebRTC in sdp

type WebRTCInSdpRequest struct {
	Sid         StreamId           `json:"id"`
	Description SessionDescription `json:"description"`
}

func NewWebRTCInSdpRequest(sid StreamId, description SessionDescription) *WebRTCInSdpRequest {
	return &WebRTCInSdpRequest{sid, description}
}

type WebRTCInSdp struct {
}

type WebRTCInSdpResponse struct {
	Data WebRTCInSdp `json:"data"`
}

// WebRTC in ice

type WebRTCInIceRequest struct {
	Sid StreamId     `json:"id"`
	Ice IceCandidate `json:"ice"`
}

func NewWebRTCInIceRequest(sid StreamId, ice IceCandidate) *WebRTCInIceRequest {
	return &WebRTCInIceRequest{sid, ice}
}

type WebRTCInIce struct {
}

type WebRTCInIceResponse struct {
	Data WebRTCInIce `json:"data"`
}
