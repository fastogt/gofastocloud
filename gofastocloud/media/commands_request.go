package media

import (
	"encoding/json"
	"errors"

	"gitlab.com/fastogt/gofastogt/gofastogt"
)

// Stop service

type StopServiceRequest struct {
	Delay uint64 `json:"delay"`
}

func NewStopServiceRequest(delay uint64) *StopServiceRequest {
	return &StopServiceRequest{delay}
}

// Inject master stream

type InjectMasterInputUrlRequest struct {
	Sid StreamId `json:"id"`
	Url InputUri `json:"url"`
}

func (inj *InjectMasterInputUrlRequest) UnmarshalJSON(data []byte) error {
	request := struct {
		Sid *StreamId `json:"id"`
		Url *InputUri `json:"url"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}

	if request.Sid == nil {
		return errors.New("id field required")
	}
	sid := *request.Sid
	if !sid.IsValid() {
		return errors.New("invalid id field")
	}

	if request.Url == nil {
		return errors.New("url field required")
	}
	inj.Sid = sid
	inj.Url = *request.Url
	return nil
}

func NewInjectMasterInputUrlRequest(sid StreamId, url InputUri) *InjectMasterInputUrlRequest { // +
	return &InjectMasterInputUrlRequest{sid, url}
}

func NewInjectMasterInputUrlRequestFromBytes(data []byte) (*InjectMasterInputUrlRequest, error) {
	var result InjectMasterInputUrlRequest
	if err := json.Unmarshal(data, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

type InjectMasterInputUrl struct {
}

type InjectMasterInputUrlResponse struct {
	Data InjectMasterInputUrl `json:"data"`
}

// Remove master stream

type RemoveMasterInputUrlRequest struct {
	Sid StreamId `json:"id"`
	Url InputUri `json:"url"`
}

func NewRemoveMasterInputUrlRequest(sid StreamId, url InputUri) *RemoveMasterInputUrlRequest { // +
	return &RemoveMasterInputUrlRequest{sid, url}
}

func NewRemoveMasterInputUrlRequestFromBytes(data []byte) (*RemoveMasterInputUrlRequest, error) {
	var result RemoveMasterInputUrlRequest
	if err := json.Unmarshal(data, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

type RemoveMasterInputUrl struct {
}

type RemoveMasterInputUrlResponse struct {
	Data RemoveMasterInputUrl `json:"data"`
}

// Get hardware hash

type HardwareHashRequest struct {
	Algo gofastogt.AlgoType `json:"algo"`
}

func NewHardwareHashRequest(algo gofastogt.AlgoType) *HardwareHashRequest { // +
	return &HardwareHashRequest{algo}
}

func NewHardwareHashRequestFromBytes(data []byte) (*HardwareHashRequest, error) {
	var result HardwareHashRequest
	if err := json.Unmarshal(data, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

type HardwareHash struct {
	Hash string `json:"key"`
}

func (hard *HardwareHash) UnmarshalJSON(data []byte) error {
	required := struct {
		Hash *string `json:"key"`
	}{}
	err := json.Unmarshal(data, &required)
	if err != nil {
		return err
	}

	if required.Hash == nil {
		return errors.New("key field required")
	}

	hash := *required.Hash
	str := gofastogt.HardwareHash(hash)
	if !str.IsValid() {
		return errors.New("invlaid hardware hash")
	}

	if !gofastogt.IsValidHardwareHashFull(str, false) {
		return errors.New("invlaid hardware hash")
	}

	hard.Hash = hash
	return nil
}

type HardwareHashResponse struct {
	Data HardwareHash `json:"data"`
}

//

type StatsResponse struct {
	Data FullServerInfo `json:"data"`
}

// Probe in stream

type ProbeInStreamRequest struct {
	Url InputUri `json:"url"`
}

func NewProbeInStreamRequest(url InputUri) *ProbeInStreamRequest { // +
	return &ProbeInStreamRequest{url}
}

func NewProbeInStreamRequestFromBytes(data []byte) (*ProbeInStreamRequest, error) {
	var result ProbeInStreamRequest
	if err := json.Unmarshal(data, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

type ProbeInStream struct {
	ProbeData
}

type ProbeInStreamResponse struct {
	Data ProbeInStream `json:"data"`
}

// Probe out stream

type ProbeOutStreamRequest struct {
	Url OutputUri `json:"url"`
}

func NewProbeOutStreamRequest(url OutputUri) *ProbeOutStreamRequest { // +
	return &ProbeOutStreamRequest{url}
}

func NewProbeOutStreamRequestFromBytes(data []byte) (*ProbeOutStreamRequest, error) {
	var result ProbeOutStreamRequest
	if err := json.Unmarshal(data, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

type ProbeOutStream struct {
	ProbeData
}

type ProbeOutStreamResponse struct {
	Data ProbeOutStream `json:"data"`
}

// Mount s3

type MountS3BucketRequest struct {
	Name   string `json:"name"`
	Path   string `json:"path"`
	Key    string `json:"key"`
	Secret string `json:"secret"`
}

func NewMountS3BucketRequest(name string, path string, key string, secret string) *MountS3BucketRequest { // +
	return &MountS3BucketRequest{name, path, key, secret}
}

func NewMountS3BucketRequestFromBytes(data []byte) (*MountS3BucketRequest, error) {
	var result MountS3BucketRequest
	if err := json.Unmarshal(data, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

type MountS3Bucket struct {
}

type MountS3BucketResponse struct {
	Data MountS3Bucket `json:"data"`
}

// Unmount s3

type UnMountS3BucketRequest struct {
	Path string `json:"path"`
}

func NewUnMountS3BucketRequest(path string) *UnMountS3BucketRequest { // +
	return &UnMountS3BucketRequest{Path: path}
}

func NewUnMountS3BucketRequestFromBytes(data []byte) (*UnMountS3BucketRequest, error) {
	var result UnMountS3BucketRequest
	if err := json.Unmarshal(data, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

type UnMountS3Bucket struct {
}

type UnMountS3BucketResponse struct {
	Data UnMountS3Bucket `json:"data"`
}

//

type ScanS3BucketsRequest struct {
}

func NewScanS3BucketsRequest() *ScanS3BucketsRequest { // +
	return &ScanS3BucketsRequest{}
}

func NewScanS3BucketsRequestFromBytes(data []byte) (*ScanS3BucketsRequest, error) {
	var result ScanS3BucketsRequest
	if err := json.Unmarshal(data, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

type Buckets struct {
	Path string `json:"path"`
	Name string `json:"name"`
}

type ScanS3Buckets struct {
	Bucket []Buckets `json:"buckets"`
}

type ScanS3BucketsResponse struct {
	Data ScanS3Buckets `json:"data"`
}

// Scan folder

type ScanFolderRequest struct {
	Directory  string   `json:"directory"`
	Extensions []string `json:"extensions"`
}

func NewScanFolderRequest(directory string, extensions []string) *ScanFolderRequest { // +
	return &ScanFolderRequest{Directory: directory, Extensions: extensions}
}

func NewScanFolderRequestFromBytes(data []byte) (*ScanFolderRequest, error) {
	var result ScanFolderRequest
	if err := json.Unmarshal(data, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (scan *ScanFolderRequest) UnmarshalJSON(data []byte) error {
	request := struct {
		Directory  string   `json:"directory"`
		Extensions []string `json:"extensions"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if len(request.Directory) == 0 {
		return errors.New("directory can't be empty")
	}
	if len(request.Extensions) == 0 {
		return errors.New("extensions can't be empty")
	}
	scan.Directory = request.Directory
	scan.Extensions = request.Extensions
	return nil
}

type ScanFolder struct {
	Files []string `json:"files"`
}

type ScanFolderResponse struct {
	Data ScanFolder `json:"data"`
}

// Start stream

type StartStreamRequest struct {
	Config json.RawMessage `json:"config"`
}

func NewStartProxyStreamRequest(config json.RawMessage) *StartStreamRequest {
	return &StartStreamRequest{Config: config}
}

func NewStartVodProxyStreamRequest(config json.RawMessage) *StartStreamRequest {
	return &StartStreamRequest{Config: config}
}

func NewStartRestreamStreamRequest(config json.RawMessage) *StartStreamRequest {
	return &StartStreamRequest{Config: config}
}

func NewStartEncodeStreamRequest(config json.RawMessage) *StartStreamRequest {
	return &StartStreamRequest{Config: config}
}

func NewStartTimeshiftPlayerRequest(config json.RawMessage) *StartStreamRequest {
	return &StartStreamRequest{Config: config}
}

func NewStartTimeshiftRecorderRequest(config json.RawMessage) *StartStreamRequest {
	return &StartStreamRequest{Config: config}
}

func NewStartCatchupRequest(config json.RawMessage) *StartStreamRequest {
	return &StartStreamRequest{Config: config}
}

func NewStartVodRelayRequest(config json.RawMessage) *StartStreamRequest {
	return &StartStreamRequest{Config: config}
}

func NewStartVodEncodeRequest(config json.RawMessage) *StartStreamRequest {
	return &StartStreamRequest{Config: config}
}

func NewStartCodRelayRequest(config json.RawMessage) *StartStreamRequest {
	return &StartStreamRequest{Config: config}
}

func NewStartCodEncodeRequest(config json.RawMessage) *StartStreamRequest {
	return &StartStreamRequest{Config: config}
}

func NewStartEventRequest(config json.RawMessage) *StartStreamRequest {
	return &StartStreamRequest{Config: config}
}

func NewStartCvDataRequest(config json.RawMessage) *StartStreamRequest {
	return &StartStreamRequest{Config: config}
}

func NewStartChangerRelayequest(config json.RawMessage) *StartStreamRequest {
	return &StartStreamRequest{Config: config}
}

func NewStartChangerEncodeRequest(config json.RawMessage) *StartStreamRequest {
	return &StartStreamRequest{Config: config}
}

func NewStartLiteStreamRequest(config json.RawMessage) *StartStreamRequest {
	return &StartStreamRequest{Config: config}
}

func NewStartMosaicStreamRequest(config json.RawMessage) *StartStreamRequest {
	return &StartStreamRequest{Config: config}
}

type StartStream struct {
}

type StartStreamResponse struct {
	Data StartStream `json:"data"`
}

// Stop stream

type StopStreamRequest struct {
	Sid   StreamId `json:"id"`
	Force bool     `json:"force"`
}

func (stop *StopStreamRequest) UnmarshalJSON(data []byte) error {
	required := struct {
		Sid   *StreamId `json:"id"`
		Force *bool     `json:"force"`
	}{}
	err := json.Unmarshal(data, &required)
	if err != nil {
		return err
	}

	if required.Sid == nil {
		return errors.New("id field required")
	}

	if len(*required.Sid) == 0 {
		return errors.New("id can't be empty")
	}

	if required.Force == nil {
		return errors.New("force field required")
	}

	stop.Sid = *required.Sid
	stop.Force = *required.Force
	return nil
}

func NewStopStreamRequest(sid StreamId, force bool) *StopStreamRequest {
	return &StopStreamRequest{sid, force}
}

func NewStopStreamRequestFromBytes(data []byte) (*StopStreamRequest, error) {
	var result StopStreamRequest
	if err := json.Unmarshal(data, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

type StopStream struct {
}

type StopStreamResponse struct {
	Data StopStream `json:"data"`
}

// Restart stream

type RestartStreamRequest struct {
	Sid StreamId `json:"id"`
}

type RestartStream struct {
}

type RestartStreamResponse struct {
	Data RestartStream `json:"data"`
}

func (restart *RestartStreamRequest) UnmarshalJSON(data []byte) error {
	required := struct {
		Sid *StreamId `json:"id"`
	}{}
	err := json.Unmarshal(data, &required)
	if err != nil {
		return err
	}

	if required.Sid == nil {
		return errors.New("id field required")
	}

	if len(*required.Sid) == 0 {
		return errors.New("id can't be empty")
	}

	restart.Sid = *required.Sid
	return nil
}

func NewRestartStreamRequest(sid StreamId) *RestartStreamRequest {
	return &RestartStreamRequest{sid}
}

func NewRestartStreamRequestFromBytes(data []byte) (*RestartStreamRequest, error) {
	var result RestartStreamRequest
	if err := json.Unmarshal(data, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

// Clean stream

type CleanStreamRequest struct {
	Config json.RawMessage `json:"config"`
}

func NewCleanStreamRequest(config json.RawMessage) *CleanStreamRequest { // +
	return &CleanStreamRequest{Config: config}
}

func NewCleanStreamRequestFromBytes(data []byte) (*CleanStreamRequest, error) {
	var clean CleanStreamRequest
	if err := json.Unmarshal(data, &clean); err != nil {
		return nil, err
	}
	return &clean, nil
}

type CleanStream struct {
}

type CleanStreamResponse struct {
	Data CleanStream `json:"data"`
}

// GetLog stream

type GetLogStreamRequest struct {
	Sid         StreamId `json:"id"`
	FeedbackDir string   `json:"feedback_directory"`
}

func NewGetLogStreamRequest(sid StreamId, feedbackDir string) *GetLogStreamRequest {
	return &GetLogStreamRequest{sid, feedbackDir}
}

func (log *GetLogStreamRequest) UnmarshalJSON(data []byte) error {
	request := struct {
		Sid         *StreamId `json:"id"`
		FeedbackDir *string   `json:"feedback_directory"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Sid == nil {
		return errors.New("id field required")
	}
	if len(*request.Sid) == 0 {
		return errors.New("id can't be empty")
	}
	if request.FeedbackDir == nil {
		return errors.New("feedback_directory field required")
	}
	if len(*request.FeedbackDir) == 0 {
		return errors.New("feedback_directory can't be empty")
	}

	log.Sid = *request.Sid
	log.FeedbackDir = *request.FeedbackDir
	return nil
}

// GetPipeline stream

type GetPipelineStreamRequest struct {
	Sid         StreamId `json:"id"`
	FeedbackDir string   `json:"feedback_directory"`
}

func NewGetPipelineStreamRequest(sid StreamId, feedbackDir string) *GetPipelineStreamRequest {
	return &GetPipelineStreamRequest{sid, feedbackDir}
}

func (pipe *GetPipelineStreamRequest) UnmarshalJSON(data []byte) error {
	request := struct {
		Sid         *StreamId `json:"id"`
		FeedbackDir *string   `json:"feedback_directory"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Sid == nil {
		return errors.New("id field required")
	}
	if len(*request.Sid) == 0 {
		return errors.New("id can't be empty")
	}
	if request.FeedbackDir == nil {
		return errors.New("feedback_directory field required")
	}
	if len(*request.FeedbackDir) == 0 {
		return errors.New("feedback_directory can't be empty")
	}

	pipe.Sid = *request.Sid
	pipe.FeedbackDir = *request.FeedbackDir
	return nil
}

// GetConfigJson stream

type GetConfigJsonStreamRequest struct {
	Sid         StreamId `json:"id"`
	FeedbackDir string   `json:"feedback_directory"`
}

func NewGetConfigJsonStreamRequest(sid StreamId, feedbackDir string) *GetConfigJsonStreamRequest {
	return &GetConfigJsonStreamRequest{sid, feedbackDir}
}

func (config *GetConfigJsonStreamRequest) UnmarshalJSON(data []byte) error {
	request := struct {
		Sid         *StreamId `json:"id"`
		FeedbackDir *string   `json:"feedback_directory"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Sid == nil {
		return errors.New("id field required")
	}
	if len(*request.Sid) == 0 {
		return errors.New("id can't be empty")
	}
	if request.FeedbackDir == nil {
		return errors.New("feedback_directory field required")
	}
	if len(*request.FeedbackDir) == 0 {
		return errors.New("feedback_directory can't be empty")
	}

	config.Sid = *request.Sid
	config.FeedbackDir = *request.FeedbackDir
	return nil
}

// ChangeInputStreamRequest stream

type ChangeInputStreamRequest struct {
	Sid       StreamId `json:"id"`
	ChannelId int      `json:"channel_id"`
}

func NewChangeInputStreamRequest(sid StreamId, channelId int) *ChangeInputStreamRequest {
	return &ChangeInputStreamRequest{sid, channelId}
}

func NewChangeInputStreamRequestFromBytes(data []byte) (*ChangeInputStreamRequest, error) {
	var result ChangeInputStreamRequest
	if err := json.Unmarshal(data, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (change *ChangeInputStreamRequest) UnmarshalJSON(data []byte) error {
	request := struct {
		Sid       *StreamId `json:"id"`
		ChannelId *int      `json:"channel_id"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Sid == nil {
		return errors.New("id field required")
	}
	if len(*request.Sid) == 0 {
		return errors.New("id can't be empty")
	}
	if request.ChannelId == nil {
		return errors.New("channel_id field required")
	}

	change.Sid = *request.Sid
	change.ChannelId = *request.ChannelId
	return nil
}

type ChangeInputStream struct {
}

type ChangeInputStreamResponse struct {
	Data ChangeInputStream `json:"data"`
}
