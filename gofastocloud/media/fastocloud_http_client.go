package media

import (
	"net/url"
	"sync"

	"gitlab.com/fastogt/gofastogt/gofastogt"
)

type StreamInfo struct {
	id StreamId

	Request   *StartStreamRequest // if fasto created we can save request
	Statistic *StreamStatisticInfo
}

func (info *StreamInfo) ID() StreamId {
	return info.id
}

func NewStreamInfo(id StreamId, req *StartStreamRequest, statistic *StreamStatisticInfo) *StreamInfo {
	info := StreamInfo{id: id, Request: req, Statistic: statistic}
	return &info
}

type FastoCloud struct {
	endpoint string // http://127.0.0.1:6317

	realIP *string // using for identify original sender

	api  *gofastogt.LicenseKey
	info *FullServerInfo

	isStopped bool

	activeStreamsMutex sync.Mutex
	activeStreams      map[StreamId]*StreamInfo
}

func (fasto *FastoCloud) IsActive() bool {
	return fasto.info != nil
}

func NewFastoCloud(endpoint string, apiKey *gofastogt.LicenseKey) *FastoCloud {
	return NewRealIPFastoCloud(endpoint, apiKey, nil)
}

func NewRealIPFastoCloud(endpoint string, apiKey *gofastogt.LicenseKey, realIP *string) *FastoCloud {
	return &FastoCloud{endpoint: endpoint, api: apiKey, realIP: realIP, isStopped: false,
		info: nil, activeStreamsMutex: sync.Mutex{}, activeStreams: make(map[StreamId]*StreamInfo)}
}

func (client *FastoCloud) GetApiKey() *gofastogt.LicenseKey {
	return client.api
}

func (client *FastoCloud) GetLicenseKey(sameMachine bool) *gofastogt.License {
	proj := client.GetProject()
	if proj == nil {
		return nil
	}

	lic := gofastogt.NewLicense(0)
	lic.UpdateLicense(client.GetApiKey(), sameMachine, *proj)
	return lic
}

func (client *FastoCloud) GetHttpHost() *url.URL {
	if client.info == nil {
		return nil
	}

	result, err := url.Parse(client.info.HttpHost)
	if err != nil {
		return nil
	}
	return result
}

func (client *FastoCloud) GetVodsHost() *url.URL {
	if client.info == nil {
		return nil
	}

	result, err := url.Parse(client.info.VodsHost)
	if err != nil {
		return nil
	}
	return result
}

func (client *FastoCloud) GetCodsHost() *url.URL {
	if client.info == nil {
		return nil
	}

	result, err := url.Parse(client.info.CodsHost)
	if err != nil {
		return nil
	}
	return result
}

func (client *FastoCloud) GetHttpDir() *string {
	if client.info == nil {
		return nil
	}

	return &client.info.HlsDir
}

func (client *FastoCloud) GetVodsDir() *string {
	if client.info == nil {
		return nil
	}

	return &client.info.VodsDir
}

func (client *FastoCloud) GetCodsDir() *string {
	if client.info == nil {
		return nil
	}

	return &client.info.CodsDir
}

func (client *FastoCloud) GetTimeshiftsDir() *string {
	if client.info == nil {
		return nil
	}

	return &client.info.TimeshiftsDir
}

func (client *FastoCloud) GetFeedbackDir() *string {
	if client.info == nil {
		return nil
	}

	return &client.info.FeedbackDir
}

func (client *FastoCloud) GetProxyDir() *string {
	if client.info == nil {
		return nil
	}

	return &client.info.ProxyDir
}

func (client *FastoCloud) GetDataDir() *string {
	if client.info == nil {
		return nil
	}

	return &client.info.DataDir
}

func (client *FastoCloud) GetVSystem() *string {
	if client.info == nil {
		return nil
	}

	return &client.info.VSystem
}

func (client *FastoCloud) GetVRole() *string {
	if client.info == nil {
		return nil
	}

	return &client.info.VRole
}

func (client *FastoCloud) GetProject() *string {
	if client.info == nil {
		return nil
	}

	return &client.info.Project
}

func (client *FastoCloud) GetVersion() *string {
	if client.info == nil {
		return nil
	}

	return &client.info.Version
}

func (client *FastoCloud) GetExpirationTime() *gofastogt.UtcTimeMsec {
	if client.info == nil {
		return nil
	}

	return &client.info.ExpireTime
}

func (client *FastoCloud) GetRuntimeStats() *ServiceStatisticInfo {
	if client.info == nil {
		return nil
	}

	return &client.info.Runtime
}

func (client *FastoCloud) GetOS() *gofastogt.OperationSystem {
	if client.info == nil {
		return nil
	}

	return &client.info.OS
}

func (client *FastoCloud) IsPro() bool {
	if client.info == nil {
		return false
	}
	return client.info.IsPro()
}

func (fasto *FastoCloud) deleteActiveStream(sid StreamId) {
	fasto.activeStreamsMutex.Lock()
	delete(fasto.activeStreams, sid)
	fasto.activeStreamsMutex.Unlock()
}

func (fasto *FastoCloud) ActiveStreamsStats() []StreamStatisticInfo {
	stats := make([]StreamStatisticInfo, 0)
	fasto.activeStreamsMutex.Lock()
	for _, v := range fasto.activeStreams {
		if v.Statistic != nil {
			stats = append(stats, *v.Statistic)
		}
	}
	fasto.activeStreamsMutex.Unlock()
	return stats
}

func (fasto *FastoCloud) FindActiveStreamStats(sid StreamId) *StreamInfo {
	fasto.activeStreamsMutex.Lock()
	defer fasto.activeStreamsMutex.Unlock()
	found, ok := fasto.activeStreams[sid]
	if !ok {
		return nil
	}
	return found
}
