package media

import (
	"encoding/json"
	"errors"

	"gitlab.com/fastogt/gofastogt/gofastogt"
)

type MediaServerStaticInfo struct {
	gofastogt.LicensedProjectInfo
	OS gofastogt.OperationSystem `json:"os"`

	HttpHost string `json:"hls_host"`
	VodsHost string `json:"vods_host"`
	CodsHost string `json:"cods_host"`

	HlsDir  string `json:"hls_dir"`
	VodsDir string `json:"vods_dir"`
	CodsDir string `json:"cods_dir"`

	TimeshiftsDir string `json:"timeshifts_dir"`
	FeedbackDir   string `json:"feedback_dir"`
	ProxyDir      string `json:"proxy_dir"`
	DataDir       string `json:"data_dir"`

	// virtual
	VSystem string `json:"vsystem"`
	VRole   string `json:"vrole"`
}

func (med *MediaServerStaticInfo) UnmarshalJSON(data []byte) error {
	req := struct {
		OS *gofastogt.OperationSystem `json:"os"`

		HttpHost *string `json:"hls_host"`
		VodsHost *string `json:"vods_host"`
		CodsHost *string `json:"cods_host"`

		HlsDir  *string `json:"hls_dir"`
		VodsDir *string `json:"vods_dir"`
		CodsDir *string `json:"cods_dir"`

		TimeshiftsDir *string `json:"timeshifts_dir"`
		FeedbackDir   *string `json:"feedback_dir"`
		ProxyDir      *string `json:"proxy_dir"`
		DataDir       *string `json:"data_dir"`

		// virtual
		VSystem *string `json:"vsystem"`
		VRole   *string `json:"vrole"`
	}{}

	var lic gofastogt.LicensedProjectInfo
	err := json.Unmarshal(data, &lic)
	if err != nil {
		return err
	}

	err = json.Unmarshal(data, &req)
	if err != nil {
		return err
	}

	if req.OS == nil {
		return errors.New("os field required")
	}
	if req.HttpHost == nil {
		return errors.New("hls_host field required")
	}
	if req.VodsHost == nil {
		return errors.New("vods_host field required")
	}
	if req.CodsHost == nil {
		return errors.New("cods_host field required")
	}
	if req.HlsDir == nil {
		return errors.New("hls_dir field required")
	}
	if req.VodsDir == nil {
		return errors.New("vods_dir field required")
	}
	if req.CodsDir == nil {
		return errors.New("cods_dir field required")
	}
	if req.TimeshiftsDir == nil {
		return errors.New("timeshifts_dir field required")
	}
	if req.FeedbackDir == nil {
		return errors.New("feedback_dir field required")
	}
	if req.ProxyDir == nil {
		return errors.New("proxy_dir field required")
	}
	if req.DataDir == nil {
		return errors.New("data_dir field required")
	}
	if req.VSystem == nil {
		return errors.New("vsystem field required")
	}
	if req.VRole == nil {
		return errors.New("vrole field required")
	}

	med.LicensedProjectInfo = lic
	med.OS = *req.OS
	med.HttpHost = *req.HttpHost
	med.VodsHost = *req.VodsHost
	med.CodsHost = *req.CodsHost

	med.HlsDir = *req.HlsDir
	med.VodsDir = *req.VodsDir
	med.CodsDir = *req.CodsDir
	med.TimeshiftsDir = *req.TimeshiftsDir

	med.FeedbackDir = *req.FeedbackDir
	med.ProxyDir = *req.ProxyDir
	med.DataDir = *req.DataDir

	med.VSystem = *req.VSystem
	med.VRole = *req.VRole

	return nil
}

const (
	FASTOCLOUD_COM    string = "fastocloud"
	FASTOCLOUD_PRO    string = "fastocloud_pro"
	FASTOCLOUD_PRO_ML string = "fastocloud_pro_ml"
)

type FullServerInfo struct {
	MediaServerStaticInfo
	Runtime ServiceStatisticInfo
}

func (machine *FullServerInfo) UnmarshalJSON(data []byte) error {
	var med MediaServerStaticInfo
	err := json.Unmarshal(data, &med)
	if err != nil {
		return err
	}

	var mach ServiceStatisticInfo
	err = json.Unmarshal(data, &mach)
	if err != nil {
		return err
	}

	machine.MediaServerStaticInfo = med
	machine.Runtime = mach
	return nil
}

func (info *FullServerInfo) IsPro() bool {
	return (info.Project == FASTOCLOUD_PRO || info.Project == FASTOCLOUD_PRO_ML)
}
