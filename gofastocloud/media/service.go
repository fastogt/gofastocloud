package media

import (
	"encoding/json"
	"errors"

	"gitlab.com/fastogt/gofastogt/gofastogt"
)

type OnlineUsers struct {
	Daemon int `json:"daemon"`
	Http   int `json:"http"`
	Vods   int `json:"vods"`
	Cods   int `json:"cods"`
}

type ServiceStatisticInfo struct {
	gofastogt.Machine
	OnlineUsers OnlineUsers `json:"online_users"`
}

// #FIXME: i don't know how it works, why we should 2x Unmarshal
func (stat *ServiceStatisticInfo) UnmarshalJSON(data []byte) error {
	req := struct {
		OnlineUsers *OnlineUsers `json:"online_users"`
	}{}

	err := json.Unmarshal(data, &req)
	if err != nil {
		return err
	}

	if req.OnlineUsers == nil {
		return errors.New("online_users field required")
	}

	var mach gofastogt.Machine
	err = json.Unmarshal(data, &mach)
	if err != nil {
		return err
	}

	stat.OnlineUsers = *req.OnlineUsers
	stat.Machine = mach
	return nil
}

func (online *OnlineUsers) UnmarshalJSON(data []byte) error {
	req := struct {
		Daemon *int `json:"daemon"`
		Http   *int `json:"http"`
		Vods   *int `json:"vods"`
		Cods   *int `json:"cods"`
	}{}

	err := json.Unmarshal(data, &req)
	if err != nil {
		return err
	}

	if req.Daemon == nil {
		return errors.New("daemon field required")
	}
	if req.Http == nil {
		return errors.New("http field required")
	}
	if req.Vods == nil {
		return errors.New("vods field required")
	}
	if req.Cods == nil {
		return errors.New("cods field required")
	}

	online.Daemon = *req.Daemon
	online.Http = *req.Http
	online.Vods = *req.Vods
	online.Cods = *req.Cods
	return nil
}

type CDNStatisticInfo struct {
	Id               string                `json:"id"`
	RSS              int64                 `json:"rss"`
	Cpu              float64               `json:"cpu"`
	Timestamp        gofastogt.UtcTimeMsec `json:"timestamp"`
	LastUpdate       gofastogt.UtcTimeMsec `json:"last_update"`
	OnlineClients    int64                 `json:"online_clients"`
	RequestsCount    int64                 `json:"requests_count"`
	ConnectionsCount int64                 `json:"connections_count"`
}

type PingInfo struct {
	Timestamp gofastogt.UtcTimeMsec `json:"timestamp"`
}

type InputStreamStatisticInfo struct {
	Id             int                   `json:"id"`
	LastUpdateTime gofastogt.UtcTimeMsec `json:"last_update_time"`
	PrevTotalBytes int64                 `json:"prev_total_bytes"`
	TotalBytes     int64                 `json:"total_bytes"`
	Bps            int32                 `json:"bps"`
	Dbps           string                `json:"dbps"`
}

type OutputStreamStatisticInfo struct {
	Id             int                   `json:"id"`
	LastUpdateTime gofastogt.UtcTimeMsec `json:"last_update_time"`
	PrevTotalBytes int64                 `json:"prev_total_bytes"`
	TotalBytes     int64                 `json:"total_bytes"`
	Bps            int32                 `json:"bps"`
	Dbps           string                `json:"dbps"`
}
