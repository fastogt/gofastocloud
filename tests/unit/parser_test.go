package unittests

import (
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/fastocloud/gofastocloud/gofastocloud/media/m3u"
)

func TestShortM3uParse(t *testing.T) {
	data, err := os.ReadFile("short.m3u")
	assert.NoError(t, err)

	play, err := m3u.ParseToPlaylist(string(data))

	assert.NoError(t, err)
	assert.Equal(t, len(play.Tracks), 7)

	assert.Equal(t, play.Tracks[5].Name, "Juego de tronos S01 E01")
	assert.Equal(t, play.Tracks[5].URI, "http://00322.xyz:8000/series/EcuaPlay@VodS22/3Il7@32M2n1IlbIl/135920.mp4")
	assert.Equal(t, len(play.Tracks[5].Tags), 0)

	assert.Equal(t, play.Tracks[0].URI, "http://00322.xyz:8000/EcuaPlay@VodS22/3Il7@32M2n1IlbIl/4159")
	assert.Equal(t, play.Tracks[2].Name, "La playlist S01 E03")

	temp := strings.Split(play.Tracks[3].URI, ".")
	assert.Equal(t, temp[len(temp)-1], "mkv")
}

func TestLongM3uParse(t *testing.T) {
	data, err := os.ReadFile("long.m3u")
	assert.NoError(t, err)

	play, err := m3u.ParseToPlaylist(string(data))

	assert.NoError(t, err)
	assert.Equal(t, len(play.Tracks), 7)

	assert.Equal(t, play.Tracks[3].Name, "Prison Break  S01 Prison Break.S01.E22")
	assert.Equal(t, play.Tracks[3].URI, "http://snaptv.watch:80/series/258309/689911/136616.mkv")
	assert.Equal(t, len(play.Tracks[3].Tags), 3)

	assert.Equal(t, play.Tracks[0].URI, "http://snaptv.watch:80/258309/689911/261433")
	assert.Equal(t, play.Tracks[2].Name, "ledig")

	temp := strings.Split(play.Tracks[3].URI, ".")
	assert.Equal(t, temp[len(temp)-1], "mkv")
}
