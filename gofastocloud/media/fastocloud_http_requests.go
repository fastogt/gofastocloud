package media

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"

	"gitlab.com/fastogt/gofastogt/gofastogt"
)

// GET requests

func (fasto *FastoCloud) GetStats() (*FullServerInfo, error) {
	url := fmt.Sprintf("/%s", kGetStatsCommand)
	req, err := fasto.makeHttpGetRequest(url)
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response StatsResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	full := response.Data
	fasto.info = &full
	return &full, nil
}

func (fasto *FastoCloud) GetLogService() ([]byte, error) {
	url := fmt.Sprintf("/%s", kGetLogServiceCommand)
	req, err := fasto.makeHttpGetRequest(url)
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	return io.ReadAll(resp.Body)
}

func (fasto *FastoCloud) ScanS3Buckets() (*ScanS3Buckets, error) {
	url := fmt.Sprintf("/%s", kScanS3BucketsCommand)
	req, err := fasto.makeHttpGetRequest(url)
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response ScanS3BucketsResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

// POST requests

func (fasto *FastoCloud) ScanFolder(request *ScanFolderRequest) (*ScanFolder, error) {
	params, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("/%s", kScanFolderCommand)
	req, err := fasto.makeHttpPostRequest(url, "application/json", bytes.NewBuffer(params))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response ScanFolderResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) GetHardwareHash(request *HardwareHashRequest) (*HardwareHash, error) {
	params, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("/%s", kGetHardwareHashCommand)
	req, err := fasto.makeHttpPostRequest(url, "application/json", bytes.NewBuffer(params))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response HardwareHashResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) ChangeInputStream(request *ChangeInputStreamRequest) (*ChangeInputStream, error) {
	params, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("/%s", kChangeInputStreamCommand)
	req, err := fasto.makeHttpPostRequest(url, "application/json", bytes.NewBuffer(params))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response ChangeInputStreamResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) RemoveMasterInputUrl(request *RemoveMasterInputUrlRequest) (*RemoveMasterInputUrl, error) {
	params, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("/%s", kRemoveMasterInputUrl)
	req, err := fasto.makeHttpPostRequest(url, "application/json", bytes.NewBuffer(params))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response RemoveMasterInputUrlResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) InjectMasterInputUrl(request *InjectMasterInputUrlRequest) (*InjectMasterInputUrl, error) {
	params, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("/%s", kInjectMasterInputUrl)
	req, err := fasto.makeHttpPostRequest(url, "application/json", bytes.NewBuffer(params))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response InjectMasterInputUrlResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) GetLogStream(request *GetLogStreamRequest) ([]byte, error) {
	params, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("/%s", kGetLogStreamCommand)
	req, err := fasto.makeHttpPostRequest(url, "application/json", bytes.NewBuffer(params))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	return io.ReadAll(resp.Body)
}

func (fasto *FastoCloud) GetPipelineStream(request *GetPipelineStreamRequest) ([]byte, error) {
	params, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("/%s", kGetPipelineStreamCommand)
	req, err := fasto.makeHttpPostRequest(url, "application/json", bytes.NewBuffer(params))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	return io.ReadAll(resp.Body)
}

func (fasto *FastoCloud) GetConfigStream(request *GetConfigJsonStreamRequest) ([]byte, error) {
	params, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("/%s", kGetConfigJsonStreamCommand)
	req, err := fasto.makeHttpPostRequest(url, "application/json", bytes.NewBuffer(params))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	return io.ReadAll(resp.Body)
}

func (fasto *FastoCloud) StartStream(request *StartStreamRequest) (*StartStream, error) {
	params, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	type Config struct {
		Id *StreamId `json:"id"`
	}

	config := struct {
		Config *Config `json:"config"`
	}{}

	err = json.Unmarshal(params, &config)
	if err != nil {
		return nil, err
	}

	if config.Config == nil {
		return nil, errors.New("config field required")
	}

	if config.Config.Id == nil {
		return nil, errors.New("id field required")
	}

	sid := *config.Config.Id
	if len(sid) == 0 {
		return nil, errors.New("id can't be empty")
	}

	url := fmt.Sprintf("/%s", kStartStreamCommand)
	req, err := fasto.makeHttpPostRequest(url, "application/json", bytes.NewBuffer(params))
	if err != nil {
		return nil, err
	}

	fasto.activeStreamsMutex.Lock()
	defer fasto.activeStreamsMutex.Unlock()

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response StartStreamResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	if _, ok := fasto.activeStreams[sid]; ok {
	} else {
		fasto.activeStreams[sid] = NewStreamInfo(sid, request, nil)
	}
	return &response.Data, nil
}

func (fasto *FastoCloud) CleanStream(request *CleanStreamRequest) (*CleanStream, error) {
	params, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("/%s", kCleanStreamCommand)
	req, err := fasto.makeHttpPostRequest(url, "application/json", bytes.NewBuffer(params))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response CleanStreamResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) ProbeInStream(request *ProbeInStreamRequest) (*ProbeInStream, error) {
	params, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("/%s", kProbeInStreamCommand)
	req, err := fasto.makeHttpPostRequest(url, "application/json", bytes.NewBuffer(params))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response ProbeInStreamResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) ProbeOutStream(request *ProbeOutStreamRequest) (*ProbeOutStream, error) {
	params, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("/%s", kProbeOutStreamCommand)
	req, err := fasto.makeHttpPostRequest(url, "application/json", bytes.NewBuffer(params))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response ProbeOutStreamResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) RestartStream(request *RestartStreamRequest) (*RestartStream, error) {
	params, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("/%s", kRestartStreamCommand)
	req, err := fasto.makeHttpPostRequest(url, "application/json", bytes.NewBuffer(params))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response RestartStreamResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) StopStream(request *StopStreamRequest) (*StopStream, error) {
	params, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("/%s", kStopStreamCommand)
	req, err := fasto.makeHttpPostRequest(url, "application/json", bytes.NewBuffer(params))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response StopStreamResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) MountS3Bucket(request *MountS3BucketRequest) (*MountS3Bucket, error) {
	params, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("/%s", kMountS3BucketCommand)
	req, err := fasto.makeHttpPostRequest(url, "application/json", bytes.NewBuffer(params))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response MountS3BucketResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) UnMountS3Bucket(request *UnMountS3BucketRequest) (*UnMountS3Bucket, error) {
	params, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("/%s", kUnMountS3BucketCommand)
	req, err := fasto.makeHttpPostRequest(url, "application/json", bytes.NewBuffer(params))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response UnMountS3BucketResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) WebRTCInDeinitStream(request *WebRTCInDeInitRequest) (*WebRTCInDeInit, error) {
	params, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("/%s", kWebRTCInDeinitStreamCommand)
	req, err := fasto.makeHttpPostRequest(url, "application/json", bytes.NewBuffer(params))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response WebRTCInDeInitResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) WebRTCInSDPStream(request *WebRTCInSdpRequest) (*WebRTCInSdp, error) {
	params, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("/%s", kWebRTCInSdpStreamCommand)
	req, err := fasto.makeHttpPostRequest(url, "application/json", bytes.NewBuffer(params))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response WebRTCInSdpResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) WebRTCInICEStream(request *WebRTCInIceRequest) (*WebRTCInIce, error) {
	params, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("/%s", kWebRTCInIceStreamCommand)
	req, err := fasto.makeHttpPostRequest(url, "application/json", bytes.NewBuffer(params))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response WebRTCInIceResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) WebRTCOutInitStream(request *WebRTCOutInitRequest) (*WebRTCOutInit, error) {
	params, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("/%s", kWebRTCOutInitStreamCommand)
	req, err := fasto.makeHttpPostRequest(url, "application/json", bytes.NewBuffer(params))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response WebRTCOutInitResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) WebRTCOutDeinitStream(request *WebRTCOutDeInitRequest) (*WebRTCOutDeInit, error) {
	params, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("/%s", kWebRTCOutDeinitStreamCommand)
	req, err := fasto.makeHttpPostRequest(url, "application/json", bytes.NewBuffer(params))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response WebRTCOutDeInitResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) WebRTCOutSDPStream(request *WebRTCOutSdpRequest) (*WebRTCOutSdp, error) {
	params, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("/%s", kWebRTCOutSdpStreamCommand)
	req, err := fasto.makeHttpPostRequest(url, "application/json", bytes.NewBuffer(params))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response WebRTCOutSdpResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) WebRTCOutICEStream(request *WebRTCOutIceRequest) (*WebRTCOutIce, error) {
	params, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("/%s", kWebRTCOutIceStreamCommand)
	req, err := fasto.makeHttpPostRequest(url, "application/json", bytes.NewBuffer(params))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response WebRTCOutIceResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

func (fasto *FastoCloud) WebRTCInInitStream(request *WebRTCInInitRequest) (*WebRTCInInit, error) {
	params, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("/%s", kWebRTCInInitStreamCommand)
	req, err := fasto.makeHttpPostRequest(url, "application/json", bytes.NewBuffer(params))
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	decoder := json.NewDecoder(resp.Body)
	if err = checkErrorResponse(decoder, resp); err != nil {
		return nil, err
	}

	var response WebRTCInInitResponse
	err = decoder.Decode(&response)
	if err != nil {
		return nil, err
	}

	return &response.Data, nil
}

// private:

func (fasto *FastoCloud) generateRoute(path string) string {
	return fasto.endpoint + path
}

func (fasto *FastoCloud) makeHttpPostRequest(route string, contentType string, body io.Reader) (*http.Request, error) {
	req, err := http.NewRequest("POST", fasto.generateRoute(route), body)
	if err != nil {
		return nil, err
	}

	if fasto.realIP != nil {
		req.Header.Set("X-Real-IP", *fasto.realIP)
	}

	req.Header.Add("Content-Type", contentType)
	if fasto.api != nil {
		req.Header.Set("API-KEY", string(*fasto.api))
	}

	return req, nil
}

func (fasto *FastoCloud) makeHttpPutRequest(route string, contentType string, body io.Reader) (*http.Request, error) {
	req, err := http.NewRequest("PUT", fasto.generateRoute(route), body)
	if err != nil {
		return nil, err
	}

	if fasto.realIP != nil {
		req.Header.Set("X-Real-IP", *fasto.realIP)
	}

	req.Header.Add("Content-Type", contentType)
	if fasto.api != nil {
		req.Header.Set("API-KEY", string(*fasto.api))
	}

	return req, nil
}

func (fasto *FastoCloud) makeHttpPatchRequest(route string, contentType string, body io.Reader) (*http.Request, error) {
	req, err := http.NewRequest("PATCH", fasto.generateRoute(route), body)
	if err != nil {
		return nil, err
	}

	if fasto.realIP != nil {
		req.Header.Set("X-Real-IP", *fasto.realIP)
	}

	req.Header.Add("Content-Type", contentType)
	if fasto.api != nil {
		req.Header.Set("API-KEY", string(*fasto.api))
	}

	return req, nil
}

func (fasto *FastoCloud) makeHttpGetRequest(route string) (*http.Request, error) {
	req, err := http.NewRequest("GET", fasto.generateRoute(route), nil)
	if err != nil {
		return nil, err
	}

	if fasto.realIP != nil {
		req.Header.Set("X-Real-IP", *fasto.realIP)
	}

	if fasto.api != nil {
		req.Header.Set("API-KEY", string(*fasto.api))
	}

	return req, nil
}

func (fasto *FastoCloud) makeHttpDeleteRequest(route string) (*http.Request, error) {
	req, err := http.NewRequest("DELETE", fasto.generateRoute(route), nil)
	if err != nil {
		return nil, err
	}

	if fasto.realIP != nil {
		req.Header.Set("X-Real-IP", *fasto.realIP)
	}

	if fasto.api != nil {
		req.Header.Set("API-KEY", string(*fasto.api))
	}

	return req, nil
}

func checkErrorResponse(decoder *json.Decoder, response *http.Response) error {
	if response.StatusCode != http.StatusOK {
		var eresponse gofastogt.ErrorResponse
		err := decoder.Decode(&eresponse)
		if err != nil {
			return err
		}
		return fmt.Errorf("wrong response status: %d, error: %v", response.StatusCode, eresponse.Error)
	}
	return nil
}
